# Goose game kata

Description of the kata can be found [here](GooseGame.md)

## Setup

Use the integrated gradle wrapper to run
```bash
chmod u+x gradlew
```

## Run game
```bash
./gradlew :run -q --console=plain
```

## Run tests
```bash
./gradlew :test
```