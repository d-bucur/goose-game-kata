import scala.io.StdIn
import InputCmds._

import scala.util._

object Main {
  var game = new GooseGame

  def gameCycle() {
    var continue = true

    while(continue) {
      val cmd = StdIn.readLine()

      try {
        cmd match {
          case ADDPLAYER(name) =>
            game.addPlayer(name) match {
              case Success(players) =>
                print("players: ")
                println(players.toList.map(_.name).mkString(", "))
              case Failure(ex) =>
                println(ex.getMessage)
            }
          case MOVE(name, d1, d2) =>
            val res = game.move(name, d1.toInt, d2.toInt)
            println(res)
          case MOVE_RANDOM(name) =>
            val res = game.move(name)
            println(res)
          case "" | null =>
            continue = false
          case _ =>
            println("Unknown command")
        }
      } catch {
        case e: Exception =>
          println("Unknown error")
          System.err.println(e)
      }
    }
  }

  def main(args: Array[String]): Unit = {
    gameCycle()
  }
}
