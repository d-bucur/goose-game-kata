object InputCmds {
  val ADDPLAYER = "add player (.*)".r
  val MOVE = "move (\\w+) (\\d+), (\\d+)".r
  val MOVE_RANDOM = "move (\\w+)".r
}
