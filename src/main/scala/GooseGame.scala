import scala.util._

class GooseGame {
  case class Player(name: String)

  var _players: Set[Player] = Set.empty
  var _positions: Map[String, Int] = Map.empty
  var _nextMove: (Int, Int) = generateNext()

  def addPlayer(name: String): Try[Set[Player]] = {
    val newPlayer = Player(name)
    if (_players.contains(newPlayer))
      return Failure(new IllegalArgumentException(name + ": already existing player"))
    else {
      _players = _players + newPlayer
      _positions = _positions.updated(name, 0)
      return Success(_players)
    }
  }

  def move(name: String): String = {
    val res = move(name, _nextMove._1, _nextMove._2)
    _nextMove = generateNext()
    res
  }

  def move(name: String, d1: Integer, d2: Integer): String = {
    var from = _positions(name)
    var to = from + d1 + d2
    val cappedTo = Math.min(WIN_POS, to)
    var finalDesc = s"$name rolls $d1, $d2. $name moves from ${startPosName(from)} to ${posName(cappedTo)}"
    var stopped = false
    // continue applying movement rules until none left to apply
    while (!stopped) {
      applyRules(name, from, to) match {
        case Some((newTo, description)) =>
          from = to
          to = newTo
          finalDesc += description
        case None =>
          stopped = true
      }
    }
    if (to == WIN_POS)
      finalDesc += s". $name Wins!!"
    _positions = _positions.updated(name, to)
    finalDesc
  }

  private val WIN_POS = 63
  private val BRIDGE_POS = 6
  private val GOOSE_POS = List(5, 9, 14, 18, 23, 27)

  /**
    * from => applyCondition
    */
  private type MoveCondition = Int => Boolean
  /**
    * name, from, to => to, message
    */
  private type MoveAction = (String, Int, Int) => (Int, String)

  private val movementRules: List[(MoveCondition, MoveAction)] = List(
    (_ == BRIDGE_POS, bridgeRule),
    (GOOSE_POS.contains, gooseRule),
    (_ > WIN_POS, bounceRule),
    (otherPlayerIsPresent, moveOtherPlayerRule)
  )

  private def applyRules(name: String, from: Int, to: Int): Option[(Int, String)] = {
    // iterate all rules until it find the first applicable and then apply it
    movementRules.find {
      case (cond, _) => cond(to)
    }.map {
      case (_, rule) => rule(name, from, to)
    }
  }

  private def bounceRule(name: String, from: Int, to: Int) = {
    val bounce = to - WIN_POS
    val newTo = WIN_POS - bounce
    (newTo, s". $name bounces! $name returns to ${posName(newTo)}")
  }

  private def bridgeRule(name: String, from: Int, to: Int) = {
    val newTo = 12
    (newTo, s". $name jumps to $newTo")
  }

  private def gooseRule(name: String, from: Int, to: Int) = {
    val delta = to - from
    val newTo = to + delta
    (newTo, s". $name moves again and goes to ${posName(newTo)}")
  }

  private def otherPlayerIsPresent(pos: Int): Boolean = {
    for ((_, p) <- _positions) {
      if (p == pos)
        return true
    }
    return false
  }

  private def moveOtherPlayerRule(name: String, from: Int, to: Int) = {
    var otherName = ""
    for ((k, p) <- _positions)
      if (p == to) {
        otherName = k
      }
    _positions = _positions.updated(otherName, from)
    val newTo = to
    (newTo, s". On $to there is $otherName, who returns to $from")
  }

  private def posName(from: Int): String = {
    from match {
      case 0 => "Start"
      case BRIDGE_POS => BRIDGE_POS + ", The Bridge"
      case g if GOOSE_POS.contains(g) => g + ", The Goose"
      case _ => from.toString
    }
  }

  private def startPosName(i: Int): String = {
    i match {
      case 0 => "Start"
      case _ => i.toString
    }
  }

  private def generateNext() = {
    (Random.nextInt(6)+1, Random.nextInt(6)+1)
  }
}
