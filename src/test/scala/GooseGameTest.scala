import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GooseGameTest extends FlatSpec {
  behavior of "Goose game"

  it should "add players" in {
    val cmds =
      """add player Pippo
        |add player Pluto
        |add player Pippo
        |""".stripMargin
    val outStream = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(outStream.toString ==
      """players: Pippo
        |players: Pippo, Pluto
        |Pippo: already existing player
        |""".stripMargin)
  }

  it should "move a player" in {
    val cmds =
      """move Pippo 4, 3
        |move Pluto 2, 2
        |move Pippo 2, 3
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
    """Pippo rolls 4, 3. Pippo moves from Start to 7
      |Pluto rolls 2, 2. Pluto moves from Start to 4
      |Pippo rolls 2, 3. Pippo moves from 7 to 12
      |""".stripMargin)
  }

  it should "win" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 60)
    val cmds =
      """move Pippo 1, 2
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!
        |""".stripMargin)
  }

  it should "bounce" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 60)
    val cmds =
      """move Pippo 3, 2
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61
        |""".stripMargin)
  }

  it should "roll dice" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 4)
    Main.game._nextMove = (1,2)
    val cmds =
      """move Pippo
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 1, 2. Pippo moves from 4 to 7
        |""".stripMargin)
  }

  it should "jump on bridge" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 4)
    Main.game._nextMove = (1,1)
    val cmds =
      """move Pippo
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 1, 1. Pippo moves from 4 to 6, The Bridge. Pippo jumps to 12
        |""".stripMargin)
  }

  it should "single goose jump" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 3)
    Main.game._nextMove = (1,1)
    val cmds =
      """move Pippo
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7
        |""".stripMargin)
  }

  it should "multiple goose jump" in {
    Main.game._positions = Main.game._positions.updated("Pippo", 10)
    Main.game._nextMove = (2,2)
    val cmds =
      """move Pippo
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22
        |""".stripMargin)
  }

  it should "prank other player" in {
    import Main.game
    game.addPlayer("Pippo")
    game.addPlayer("Pluto")
    game._positions = Main.game._positions.updated("Pippo", 15)
    game._positions = Main.game._positions.updated("Pluto", 17)
    game._nextMove = (1,1)
    val cmds =
      """move Pippo
        |""".stripMargin
    val out = mockConsoleStreams(cmds)
    Main.gameCycle()
    assert(out.toString ==
      """Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15
        |""".stripMargin)
  }

  private def mockConsoleStreams(inputCmds: String): ByteArrayOutputStream = {
    val outStream = new java.io.ByteArrayOutputStream()
    Console.setOut(outStream)
    val inStream = new ByteArrayInputStream(inputCmds.getBytes)
    Console.setIn(inStream)
    outStream
  }
}
